set :application, 'rnd-dev'
set :repo_url, 'git@bitbucket.org:allanchristiancarlos/rnd-dev.git'

# Branch options
# Prompts for the branch name (defaults to current branch)
#ask :branch, -> { `git rev-parse --abbrev-ref HEAD`.chomp }

# Hardcodes branch to always be master
# This could be overridden in a stage config file
set :branch, :master

# Use :debug for more verbose output when troubleshooting
set :log_level, :info

# Apache users with .htaccess files:
# it needs to be added to linked_files so it persists across deploys:
# set :linked_files, fetch(:linked_files, []).push('.env', 'web/.htaccess')
set :linked_files, fetch(:linked_files, []).push('.env')
set :linked_dirs, fetch(:linked_dirs, []).push('web/app/uploads')

namespace :deploy do

  desc 'Creating a symlink from web to public_html'
  task :restart do
    on roles(:app), in: :sequence, wait: 5 do
      execute "rm -rf ~/public_html && ln -fs #{current_path}/web ~/public_html"
    end
  end

  desc 'Resetting server paths'
  task :set_server_paths do 
    on roles(:app) do
      set :tmp_dir, "/home/#{host.user}/tmp"
      set :deploy_to, -> { "/home/#{host.user}/websites/#{fetch(:application)}" }
    end
  end

  desc "Creating .env file in websites/#{fetch(:application)}/shared/.env"
  task :upload_env_file do
    on roles(:db) do
      begin
        file = File.open(".env.#{fetch(:stage)}")

        upload! file, "/home/#{host.user}/websites/#{fetch(:application)}/shared/.env"

      rescue SystemCallError => bang
        puts "Please create your .env.#{fetch(:stage)}"
      end
    end
  end

end

before 'deploy:starting', 'deploy:upload_env_file'
before 'deploy:check', 'deploy:upload_env_file'
before 'deploy:starting', 'deploy:set_server_paths'
before 'deploy:check', 'deploy:set_server_paths'
after 'deploy:publishing', 'deploy:restart'
